<?php
include ('ParserFactory.php');
$file = $_POST['file'];

$parseFile = new parseFile($file);
$parseFile->parse();
class ParseFile{
    private $file;
    private $parserFactory;
    private $parser;

    public function __construct($file){
        $this->file = $file;
        $this->getFileExtension($this->file);
        $this->parserFactory = new ParserFactory();
        $this->parser = $this->parserFactory->getParser($this->getFileExtension($this->file));
    }

    public function parse(){
          $this->parser->fileToText($this->file);
    }


    function getFileExtension($file){

        return pathinfo($file, PATHINFO_EXTENSION);

    }
}