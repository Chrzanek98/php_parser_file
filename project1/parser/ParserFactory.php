<?php

require("INIParser.php");
require("XMLParser.php");
require("YAMLParser.php");
require("CSVParser.php");

class ParserFactory{


    public function getParser($fileExtension)
    {
        if ($fileExtension == "ini") {
            return new INIParser();
        }
        elseif ($fileExtension == "xml") {
            return new XMLParser();
        }
        elseif ($fileExtension == "yaml") {
            return new YAMLParser();
        }
        elseif ($fileExtension == "csv") {
            return new CSVParser();
        }
        else {
           echo "Nie ma parsera dla ".$fileExtension;
           throw new InvalidArgumentException("No parser for ".$fileExtension);
        }
    }

}
